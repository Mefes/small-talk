//
//  ContactProfileViewModel.swift
//  Small Talk
//
//  Created by Владимир Кадочников on 04.09.17.
//  Copyright © 2017 Владимир Кадочников. All rights reserved.
//
import RxSwift
import Foundation
import Alamofire
import AlamofireImage
class ContactProfileViewModel {
    
    var iconImage  = PublishSubject<Image>()
    var nickname = PublishSubject<String>()
    var fullname = PublishSubject<String>()
    var birthday = PublishSubject<String>()
    var sex = PublishSubject<String>()
    //    var studLanguage = PublishSubject<String>()
    
    
    init(userID:Int) {
        NetworkIteraction.init().getProfile(userID: userID){ (obj) in
            self.selfProfile = SelfProfile(json: obj)
        }
        
    }
    
    var selfProfile: SelfProfile?{
        didSet{
            //            guard let selfProfile = selfProfile else { return }
            DispatchQueue.main.async() {
                self.nickname.onNext(SelfProfile.nickname!)
                self.fullname.onNext(SelfProfile.fullname!)
                self.birthday.onNext(SelfProfile.birthday!)
                self.sex.onNext(SelfProfile.sex!)
                Alamofire.request(SelfProfile.iconURL!, method: .get).responseImage()
                    {response in
                        //                    print(response)
                        guard !response.result.isFailure else{return}
                        self.iconImage.onNext(response.result.value!)}
            }
            
        }
    }
}
