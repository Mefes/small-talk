//
//  RegistrationViewModel.swift
//  Small Talk
//
//  Created by Владимир Кадочников on 01.09.17.
//  Copyright © 2017 Владимир Кадочников. All rights reserved.
//

import Foundation
import RxSwift
import Alamofire
class RegistrationViewModell {
    let response: Observable<Int>
    
    init(email:Observable<String>,
         nickname: Observable<String>,
         name: Observable<String>,
         password: Observable<String>,
         confirmPassword: Observable<String>,
         age: Observable<String>,
         sex: String,
         studLanguage: Observable<String>,
         singUp: Observable<Void>){
        let user = Observable.combineLatest(email, nickname, name, password, confirmPassword, age, studLanguage){email, nickname, name, password, confirmPassword, age, studLanguage -> (String, String, String, String, String, String, String) in
            return (email, nickname, name, password, confirmPassword, age, studLanguage)
        }
        
        
        response = singUp
            .withLatestFrom(user)
            .flatMap{ (email, nickname, name, password, confirmPassword, age, studLanguage) in
                return Alamofire.request("   ", method: .post, parameters: ["username":nickname, "fullname":name, "email":email, "birthday" :age, "sex":sex, "stud_lang":studLanguage, "password": password])
                    .rx.responseJSON()
            }
            .map{data in
                return 1
        }
    }
}
