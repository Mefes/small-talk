//
//  ProfileViewModel.swift
//  Small Talk
//
//  Created by Владимир Кадочников on 25.08.17.
//  Copyright © 2017 Владимир Кадочников. All rights reserved.
//
import RxSwift
import Foundation
import Alamofire
import AlamofireImage
class ProfileViewModel {
    
    var iconImage  = PublishSubject<Image>()
    var nickname = PublishSubject<String>()
    var fullname = PublishSubject<String>()
    var birthday = PublishSubject<String>()
    var sex = PublishSubject<String>()
    //    var studLanguage = PublishSubject<String>()
    
    
    init(userID:Int) {
        NetworkIteraction.init().getProfile(userID: userID){ (obj) in
            self.selfProfile = SelfProfile(json: obj)
        }
        
//        Alamofire.request("\(URL_PATH)/\(REQUEST_GET_PROFILE)/\(userID)",method: .get).rx.responseJSON()
////            .subscribe(onNext: { response in
////                let arrayJSON = response as? [[String:Any]] ?? [[:]]
////                let json = arrayJSON[0] as [String:Any]
////                obj(json as AnyObject)
////            })
        
    }
    
    var selfProfile: SelfProfile?{
        didSet{
            DispatchQueue.main.async() {
                self.nickname.onNext(SelfProfile.nickname!)
                self.fullname.onNext(SelfProfile.fullname!)
                self.birthday.onNext(SelfProfile.birthday!)
                self.sex.onNext(SelfProfile.sex!)
                Alamofire.request("http://192.168.24.169:3000\(SelfProfile.iconURL!)", method: .get).responseImage()
                    {response in
                        guard !response.result.isFailure else{return}
                        self.iconImage.onNext(response.result.value!)}
            }
            
        }
    }
}
