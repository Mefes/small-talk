//
//  Validation.swift
//  Small Talk
//
//  Created by Владимир Кадочников on 01.09.17.
//  Copyright © 2017 Владимир Кадочников. All rights reserved.
//

import Foundation
import UIKit
extension UIViewController {
    
    func isValidEmail(email:String) -> Bool {
        let emailRegEx = "^(?:(?:(?:(?: )*(?:(?:(?:\\t| )*\\r\\n)?(?:\\t| )+))+(?: )*)|(?: )+)?(?:(?:(?:[-A-Za-z0-9!#$%&’*+/=?^_'{|}~]+(?:\\.[-A-Za-z0-9!#$%&’*+/=?^_'{|}~]+)*)|(?:\"(?:(?:(?:(?: )*(?:(?:[!#-Z^-~]|\\[|\\])|(?:\\\\(?:\\t|[ -~]))))+(?: )*)|(?: )+)\"))(?:@)(?:(?:(?:[A-Za-z0-9](?:[-A-Za-z0-9]{0,61}[A-Za-z0-9])?)(?:\\.[A-Za-z0-9](?:[-A-Za-z0-9]{0,61}[A-Za-z0-9])?)*)|(?:\\[(?:(?:(?:(?:(?:[0-9]|(?:[1-9][0-9])|(?:1[0-9][0-9])|(?:2[0-4][0-9])|(?:25[0-5]))\\.){3}(?:[0-9]|(?:[1-9][0-9])|(?:1[0-9][0-9])|(?:2[0-4][0-9])|(?:25[0-5]))))|(?:(?:(?: )*[!-Z^-~])*(?: )*)|(?:[Vv][0-9A-Fa-f]+\\.[-A-Za-z0-9._~!$&'()*+,;=:]+))\\])))(?:(?:(?:(?: )*(?:(?:(?:\\t| )*\\r\\n)?(?:\\t| )+))+(?: )*)|(?: )+)?$"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        let result = emailTest.evaluate(with: email)
        return result
    }
    
    func isPasswordSame(password: String , confirmPassword : String) -> Bool {
        if password == confirmPassword{
            return true
        }else{
            return false
        }
    }
    
    func isPasswordValid(_ password : String) -> Bool{
        let passwordTest = NSPredicate(format: "SELF MATCHES %@", "^(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9])[A-Za-z0-9]{8,}")
        return passwordTest.evaluate(with: password)
    }
    
    func isContainUpercase(text:String) -> Bool {
        let uppercase = NSPredicate(format: "SELF MATCHES %@", "^([A-Z])")
        return uppercase.evaluate(with:text)
    }
    
    func isContainlowercase(text:String) -> Bool {
        let lowercase = NSPredicate(format: "SELF MATCHES %@", "^([a-z])")
        return lowercase.evaluate(with:text)
    }
    
    func isContainNumbers(text:String) -> Bool {
        let numbers = NSPredicate(format: "SELF MATCHES %@", "^([0-9])")
        return numbers.evaluate(with:text)
    }
    
    func isSize(text:String) -> Bool {
        let size = NSPredicate(format: "SELF MATCHES %@", "^[A-Za-z0-9]{8,}")
        return size.evaluate(with:text)
    }
    func isNameLenght(name:String) -> Bool {
        if name.characters.count > 5 {
            return true
        } else {
            return false
        }
    }
    
    func isTextEmpty(text:String) -> Bool {
        if text.isEmpty {
            return true
        } else {
            return false
        }
    }
}
