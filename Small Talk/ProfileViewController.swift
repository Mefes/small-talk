//
//  ProfileViewController.swift
//  ttt
//
//  Created by Владимир Кадочников on 15.08.17.
//  Copyright © 2017 Владимир Кадочников. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
class ProfileViewController: UIViewController ,UITableViewDelegate{
    
    @IBOutlet weak var profileIcon: RoundedImageView!
    @IBOutlet weak var nicknameLabel: UILabel!
    @IBOutlet weak var fullNameLabel: UILabel!
    @IBOutlet weak var sexLabel: UILabel!
    @IBOutlet weak var ageLabel: UILabel!
    @IBOutlet weak var addLanguageButton: UIBarButtonItem!
    lazy var profileViewModel: ProfileViewModel = self.getViewModel()
    let disposeBag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //Binding the UI
        profileViewModel.iconImage.bind(to: profileIcon.rx.image).addDisposableTo(disposeBag)
        profileViewModel.nickname.bind(to: nicknameLabel.rx.text).addDisposableTo(disposeBag)
        profileViewModel.fullname.bind(to: fullNameLabel.rx.text).addDisposableTo(disposeBag)
        profileViewModel.birthday.bind(to: ageLabel.rx.text).addDisposableTo(disposeBag)
        profileViewModel.sex.bind(to: sexLabel.rx.text).addDisposableTo(disposeBag)
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 0
    }
    
    // MARK: - Navigation
    /*
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }*/
    
}

extension ProfileViewController{
    
    func getViewModel() -> ProfileViewModel{
        let viewModel = ProfileViewModel(userID: Auth.userID!)
        return viewModel
    }
}
