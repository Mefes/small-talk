//
//  PreferenceTalkViewController.swift
//  Small Talk
//
//  Created by Владимир Кадочников on 31.08.17.
//  Copyright © 2017 Владимир Кадочников. All rights reserved.
//

import UIKit

class PreferenceTalkViewController: UIViewController, UIPickerViewDataSource, UIPickerViewDelegate {
    var langDataSource:[String] = ["English","Русский","日本の","中國","Deutsch"]
    var langData:[String] = ["Английский","Русский","Японский","Китайский","Немецкий"]

    @IBOutlet weak var studLanguagePickerView: UIPickerView!
    var selectedLanguage:String?
    override func viewDidLoad() {
        super.viewDidLoad()
//        langDataSource = [SelfProfile.studLanguage!]   по нормальному отсюда летят языки, которые изучаешь
        studLanguagePickerView.dataSource = self;
        studLanguagePickerView.delegate = self;
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func Talk(_ sender: UIButton) {
        performSegue(withIdentifier: "TalkSegue", sender: nil)
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return langDataSource.count;
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return langDataSource[row]
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        selectedLanguage = langDataSource[row]
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "TalkSegue" {
//            selectedLanguage = langDataSource[studLanguagePickerView.selectedRow(inComponent: 0)]
            selectedLanguage = langData[studLanguagePickerView.selectedRow(inComponent: 0)]
            let nextScene = segue.destination as? TalkViewController
            nextScene?.studLanguage = selectedLanguage!
        }
    }
}
