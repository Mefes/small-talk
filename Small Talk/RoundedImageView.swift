//
//  CornerButton.swift
//  ttt
//
//  Created by Владимир Кадочников on 15.08.17.
//  Copyright © 2017 Владимир Кадочников. All rights reserved.
//

import UIKit

class RoundedImageView : UIImageView{
    
    override func awakeFromNib() {
        super.awakeFromNib()
//        self.image?.af_imageAspectScaled(toFit: CGSize(width:frame.width/2,height: frame.height/2))
        layer.cornerRadius = frame.height/2
        self.clipsToBounds = true
        layer.backgroundColor = UIColor(red:0.41, green:0.75, blue:0.98, alpha:1.0).cgColor
    
    // Making a circular UIView: cornerRadius = self.myUIImageView.frame.size.width / 2
    // Making a rounded UIView: cornerRadius = 10.0
}
}
class  BorderButton:UIButton{
    override func awakeFromNib() {
        layer.cornerRadius = 8
        self.clipsToBounds = true

    }

}
