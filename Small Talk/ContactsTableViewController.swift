//
//  ContactsTableViewController.swift
//  ttt
//
//  Created by Владимир Кадочников on 15.08.17.
//  Copyright © 2017 Владимир Кадочников. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class ContactsTableViewController: UITableViewController {
    let disposeBag = DisposeBag()
//    var contacts:[(id:Int,name:String,surname:String,nickname:String)] = []
    @IBOutlet weak var refreshButton:UIBarButtonItem!
    lazy var friendsViewModel: FriendsViewModel  = self.getViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        friendsViewModel.response.subscribe(onNext: {_ in
            self.tableView.reloadData()
                }
            )
            .addDisposableTo(disposeBag)
    }
      
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return Friends.profiles.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell:UITableViewCell?
        if Friends.profiles[indexPath.row].request! {
            let reqCell = tableView.dequeueReusableCell(withIdentifier: "requestContactCell", for: indexPath) as! RequestContactTableViewCell
            // Configure the cell...
            reqCell.setIcon(iconURL: Friends.profiles[indexPath.row].iconURL!)
            reqCell.request = "Запрос в друзья"
            reqCell.nickname = Friends.profiles[indexPath.row].nickname!
            reqCell.tag = 0
            cell = reqCell
        }else{
        let contactCell = tableView.dequeueReusableCell(withIdentifier: "contactCell", for: indexPath) as! ContactTableViewCell
        // Configure the cell...
        contactCell.name = Friends.profiles[indexPath.row].fullname!
        contactCell.setIcon(iconURL: Friends.profiles[indexPath.row].iconURL!)
        contactCell.studLanguage = Friends.profiles[indexPath.row].studLanguage!
        contactCell.nickname = Friends.profiles[indexPath.row].nickname!
            contactCell.tag = 1
            cell = contactCell
        }
        return cell!
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "contactSegue", sender: indexPath)
    }
    override func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        if tableView.cellForRow(at: indexPath)?.tag == 1 {
        let chatAction = UITableViewRowAction(style: .normal, title: "Chat") {
            _, indexPath in
            let alert = UIAlertController(title: "Title", message: "Строка таблицы", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Cancel", style: .cancel) { (sender: UIAlertAction) -> Void in
            })
            self.present(alert, animated: true, completion: nil)
        }
        chatAction.backgroundColor = UIColor(red:0.25, green:0.75, blue:0.95, alpha:1.0)
        
        let deleteAction = UITableViewRowAction(style: .destructive, title: "Удалить") {
            _, indexPath in
            let alert = UIAlertController(title: "Delete", message: "Вы уверены?", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Cancel", style: .cancel) { (sender: UIAlertAction) -> Void in
            })
            alert.addAction(UIAlertAction(title: "Enter", style: .default) { (sender: UIAlertAction) -> Void in
                self.friendsViewModel.removeFriend(userID: SelfProfile.userID!, friendID: Friends.profiles[indexPath.row].userID!)
                Friends.profiles.remove(at: indexPath.row)
                tableView.deleteRows(at: [indexPath], with: .fade)
            })
            self.present(alert, animated: true, completion: nil)

            
        }
        
        return [deleteAction, chatAction]
        }else{
            
            let acceptAction = UITableViewRowAction(style: .normal, title: "Add") {
                _, indexPath in
                let alert = UIAlertController(title: "Add to contact", message: "Вы уверены?", preferredStyle: .alert)
                
                alert.addAction(UIAlertAction(title: "Cancel", style: .cancel) { (sender: UIAlertAction) -> Void in
                })
                alert.addAction(UIAlertAction(title: "Ok", style: .default) { (sender: UIAlertAction) -> Void in
                    self.friendsViewModel.acceptRequest(userID: SelfProfile.userID!, friendID: Friends.profiles[indexPath.row].userID!)
//                    self.friendsViewModel.updateFriendslist(userID: SelfProfile.userID!)
//                    Friends.profiles.remove(at: indexPath.row)
//                    tableView.deleteRows(at: [indexPath], with: .fade)
                    tableView.reloadData()

                })
                self.present(alert, animated: true, completion: nil)
            }
            acceptAction.backgroundColor = UIColor(red:0.25, green:0.75, blue:0.95, alpha:1.0)
            
            let deleteAction = UITableViewRowAction(style: .destructive, title: "Delete") {
                _, indexPath in
                let alert = UIAlertController(title: "Delete", message: "Вы уверены?", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Cancel", style: .cancel) { (sender: UIAlertAction) -> Void in
                })
                alert.addAction(UIAlertAction(title: "Ok", style: .default) { (sender: UIAlertAction) -> Void in
                    self.friendsViewModel.deniedRequest(userID: SelfProfile.userID!, friendID: Friends.profiles[indexPath.row].userID!)
                   Friends.profiles.remove(at: indexPath.row)
                    tableView.deleteRows(at: [indexPath], with: .fade)
                })
                self.present(alert, animated: true, completion: nil)
                

            }
            return [deleteAction, acceptAction]
        }
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "contactSegue"{
            let model = segue.destination as? ContactProfileViewController
            let indexPath = sender as! IndexPath
            model?.fullname = Friends.profiles[indexPath.row].fullname!
            model?.nickname = Friends.profiles[indexPath.row].nickname!
            model?.sex = Friends.profiles[indexPath.row].sex!
            model?.year = Friends.profiles[indexPath.row].birthday!
            model?.setIcon(iconURL: Friends.profiles[indexPath.row].iconURL!)
        }
    }
}
extension ContactsTableViewController{
    func getViewModel() -> FriendsViewModel {
        let viewModel = FriendsViewModel(userID: Auth.userID!, refreshButton.rx.tap.asObservable())
        return viewModel
    }
}
