//
//  ProfileKeeper.swift
//  Small Talk
//
//  Created by Владимир Кадочников on 23.08.17.
//  Copyright © 2017 Владимир Кадочников. All rights reserved.
//

import Foundation
import SwiftyJSON
struct Profile {
    
    var userID: Int?
    var nickname: String?
    var fullname: String?
    var email: String?
    var birthday: String?
    var sex: String?
    var studLanguage: String?
    var iconURL: String?
    var request:Bool?
    
    static func setProfile(from json:AnyObject,isRequest:Bool = false) -> Profile{
        var profile = Profile()
        let array = JSON(json)
        profile.iconURL = array["img"].stringValue
        profile.userID = array["_id"].int
        profile.nickname = array["username"].stringValue
        profile.fullname = array["fullname"].stringValue
        profile.birthday = array["birthday"].stringValue
        profile.email = array["email"].stringValue
        profile.sex = array["sex"].stringValue
        profile.studLanguage = array["stud_lang"].stringValue
        profile.request = isRequest
        return profile
    }
    
}

struct SelfProfile  {
    
    static var userID: Int?
    static var nickname:String?
    static var fullname:String?
    static var email:String?
    static var birthday:String?
    static var sex:String?
    static var studLanguage:String?
    static var iconURL: String?
    
    init(json: AnyObject) {
        let profile = JSON(json)
        SelfProfile.iconURL = profile["img"].stringValue
        SelfProfile.userID = profile["_id"].int!
        SelfProfile.nickname = profile["username"].stringValue
        SelfProfile.fullname = profile["fullname"].stringValue
        SelfProfile.email = profile["email"].stringValue
        SelfProfile.birthday = profile["birthday"].stringValue
        SelfProfile.sex = profile["sex"].stringValue
        SelfProfile.studLanguage = profile["stud_lang"].stringValue
    }
}

