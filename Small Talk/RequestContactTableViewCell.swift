//
//  requestContactTableViewCell.swift
//  Small Talk
//
//  Created by Владимир Кадочников on 05.09.17.
//  Copyright © 2017 Владимир Кадочников. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage
class RequestContactTableViewCell: UITableViewCell {
    @IBOutlet weak var profileIcon: RoundedImageView!
    @IBOutlet weak var requestLabel: UILabel!
    @IBOutlet weak var nicknameLabel: UILabel!
    public var nickname:String{
        get{
            return nicknameLabel.text ?? "UNKNOWN"
        }set{
            nicknameLabel.text = newValue
        }
    }
    public var request:String{
        get{
            return requestLabel.text ?? "UNKNOWN"
        }set{
            requestLabel.text = newValue
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func setIcon(iconURL:String) {
        Alamofire.request(iconURL, method: HTTPMethod.get).responseImage()
            {response in
                guard !response.result.isFailure else{return}
                self.profileIcon.image=response.result.value!
        }
    }
}
