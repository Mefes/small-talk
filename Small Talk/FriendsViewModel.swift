//
//  FriendsModelView.swift
//  Small Talk
//
//  Created by Владимир Кадочников on 25.08.17.
//  Copyright © 2017 Владимир Кадочников. All rights reserved.
//

import Foundation
import RxSwift
import Alamofire
class FriendsViewModel {
    var response: Observable<Friends>
    init(userID:Int ,_ didPressButton:Observable<Void>) {
        Friends.removeAll()
        response = Observable.merge(
            Alamofire.request("http://192.168.24.169:3000/api/friendlist/\(userID)",method: HTTPMethod.get).rx.responseJSON()
            .map{ data -> Friends in
                let arrayJSON = data as? [[String:Any]] ?? [[:]]
                return Friends.init(data: arrayJSON as AnyObject)
                
            }
            ,Alamofire.request("http://192.168.24.169:3000/api/get_reqs/\(userID)",method: HTTPMethod.get).rx.responseJSON()
                .map{ data -> Friends in
                    let arrayJSON = data as? [[String:Any]] ?? [[:]]
                    return Friends.init(data: arrayJSON as AnyObject,req:true)
                    
            }
        )
        
        
        
    }
    func updateFriendslist(userID:Int){
    
        Friends.removeAll()
        response = Observable.merge(
            Alamofire.request("http://192.168.24.169:3000/api/friendlist/\(userID)",method: HTTPMethod.get).rx.responseJSON()
                .map{ data -> Friends in
                    let arrayJSON = data as? [[String:Any]] ?? [[:]]
                    return Friends.init(data: arrayJSON as AnyObject)
                    
            }
            ,Alamofire.request("http://192.168.24.169:3000/api/get_reqs/\(userID)",method: HTTPMethod.get).rx.responseJSON()
                .map{ data -> Friends in
                    let arrayJSON = data as? [[String:Any]] ?? [[:]]
                    return Friends.init(data: arrayJSON as AnyObject,req:true)
                
            }
        )

    }
    func acceptRequest(userID:Int,friendID:Int){
        Alamofire.request("http://192.168.24.169:3000/api/true_request/",method: HTTPMethod.post,parameters:["id":userID,"request_id":friendID]).rx.responseJSON()
        //        .addDisposableTo(disposeBag)
    }
    func deniedRequest(userID:Int,friendID:Int){
        Alamofire.request("http://192.168.24.169:3000/api/false_request/",method: HTTPMethod.post,parameters:["id":userID,"request_id":friendID]).rx.responseJSON()
        //        .addDisposableTo(disposeBag)
    }
//    func getRequestToFriends(){
//        
//    }
    func removeFriend(userID:Int,friendID:Int){
        Alamofire.request("http://192.168.24.169:3000/api/delete_friend/",method: HTTPMethod.post,parameters:["_id":userID,"user_del":friendID]).rx.responseJSON()//        .addDisposableTo(disposeBag)

        
    }
}
