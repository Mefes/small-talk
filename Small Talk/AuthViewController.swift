//
//  AuthViewController.swift
//  ttt
//
//  Created by Владимир Кадочников on 14.08.17.
//  Copyright © 2017 Владимир Кадочников. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

class AuthViewController: UIViewController {
    
    private let SING_UP_SEGUE = "RegistrationSegue"
    private let AUTH_SEGUE = "AuthSegue"
    let disposeBag = DisposeBag()
    
    @IBOutlet weak var nicknameTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var logInButton: UIButton!
    lazy var authViewModel: AuthViewModel = self.getViewModel()
    var activeTextField:UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        authViewModel.response
            .subscribe(onNext:{(response) in
                if response != -1{
                    self.performSegue(withIdentifier: self.AUTH_SEGUE, sender: response)
                }else{
                    let alert = UIAlertController(title:"AHTUNG",message:response.description,preferredStyle:.alert)
                    alert.addAction(UIAlertAction(title: "Action", style: .default, handler: nil))
                    self.present(alert,animated: true,completion: nil)}
            }, onError:{
                error in print(error)}
            )
            .addDisposableTo(disposeBag)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func singUp(_ sender: UIButton) {
        performSegue(withIdentifier: SING_UP_SEGUE, sender: nil)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
}

extension AuthViewController {
    func getViewModel() -> AuthViewModel {
        let viewModel = AuthViewModel(logInButton.rx.tap.asObservable(),nicknameTextField.rx.text.orEmpty.asObservable(),passwordTextField.rx.text.orEmpty.asObservable())
        return viewModel
    }
}
