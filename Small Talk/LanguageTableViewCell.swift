//
//  LanguageTableViewCell.swift
//  ttt
//
//  Created by Владимир Кадочников on 21.08.17.
//  Copyright © 2017 Владимир Кадочников. All rights reserved.
//

import UIKit

class LanguageTableViewCell: UITableViewCell {
    @IBOutlet weak var levelLabel: UILabel!
    @IBOutlet weak var studLanguageLabel: UILabel!
    @IBOutlet weak var timeStudLabel: UILabel!
    
    public var studLanguage:String{
        get{
            return studLanguageLabel.text ?? "UNKNOWN"
        }set{
            studLanguageLabel.text = newValue
        }
    }
    public var levelLang:String{
        get{
            return levelLabel.text ?? "UNKNOWN"
        }set{
            levelLabel.text = newValue
        }
    }
    public var timeStud:String{
        get{
            return timeStudLabel.text ?? "UNKNOWN"
        }set{
            timeStudLabel.text = newValue
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
