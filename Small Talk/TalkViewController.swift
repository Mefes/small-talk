//
//  TalkViewController.swift
//  Small Talk
//
//  Created by Владимир Кадочников on 28.08.17.
//  Copyright © 2017 Владимир Кадочников. All rights reserved.
//

import UIKit
import SocketIO
import JSQMessagesViewController
import Alamofire
import SwiftyJSON
class TalkViewController: JSQMessagesViewController, UIAlertViewDelegate {
    var requestID:Int?
    var typingTimer: Timer?
    var studLanguage: String = ""
    //    @IBOutlet weak var waitImageView: UIImageView!
    @IBOutlet weak var addContactBarButton: UIBarButtonItem!
    @IBOutlet weak var leaveChat: UIBarButtonItem!
    private var messages: [JSQMessage] = []{
        didSet{
            self.reloadMessagesView()
        }
    }
    var imgBackground:UIImageView?
    
    private var socket : SocketIOClient?
    
    private let incomingBubble = JSQMessagesBubbleImageFactory().incomingMessagesBubbleImage(with: UIColor(red:0.65, green:0.84, blue:0.97, alpha:1.0))
    private let outgoingBubble = JSQMessagesBubbleImageFactory().outgoingMessagesBubbleImage(with: UIColor(red:0.41, green:0.75, blue:0.98, alpha:1.0))
    
    
    @IBAction func addToContact(_ sender: UIButton) {
        Alamofire.request("http://192.168.24.169:3000/send_request",method: HTTPMethod.post,parameters: ["_id":SelfProfile.userID!,"request_id":requestID!]).response(){_ in
            self.addContactBarButton.isEnabled = false
        }
        
    }

   
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
        waitTime(false)
        socket = SocketIOClient(socketURL: URL(string: "http://192.168.24.169:3000/main")!, config: [.log(true),.compress, .connectParams(["username":SelfProfile.nickname!,"stud_lang":self.studLanguage,"_id":SelfProfile.userID!,"mobile":true])])
        socket?.on(clientEvent: .connect) {data, ack in
            self.socket?.emit("login")
        }
        self.senderId = SelfProfile.nickname
        self.senderDisplayName = SelfProfile.nickname
        socket?.on("chat start"){    (dataArray, socketAck) -> Void in
            let array = JSON(dataArray)
            if array.arrayObject != nil{
                for obj in array.arrayObject!{
                    let json = JSON(obj)
                    self.requestID  = json["id"].int
                }
                self.waitTime(true)
            }
        }
        socket?.on("message"){  (dataArray, socketAck) -> Void in
            let array = JSON(dataArray)
            if array.arrayObject != nil{
                for obj in array.arrayObject!{
                    let json = JSON(obj)
                    let username  = json["username"].stringValue
                    let msg = json["msg"].stringValue
                    if (msg != ""){
                        self.messages.append(JSQMessage(senderId: username, displayName: username, text: msg))
                    }
                }
            }
        }
        
        socket?.on("chat stop"){  (dataArray, socketAck) -> Void in
            self.messages.removeAll()
            
            self.waitTime(false)
        }
        
        socket?.on("typing"){ (response) -> Void in
            let sdf =  response.0[0] as! Bool
            if sdf == true{
                self.showTypingIndicator = true
            }else{
                self.showTypingIndicator = false
            }
            self.scrollToBottom(animated: true)
        }
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        socket?.connect()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: Convenience
    
    func reloadMessagesView() {
        self.collectionView?.reloadData()
        self.scrollToBottom(animated: true)
    }
    
    // JSQMessagesCollectionViewDataSource

    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, attributedTextForMessageBubbleTopLabelAt indexPath: IndexPath!) -> NSAttributedString! {
        return NSAttributedString(string: messages[indexPath.row].senderDisplayName!)
    }
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, layout collectionViewLayout: JSQMessagesCollectionViewFlowLayout!, heightForMessageBubbleTopLabelAt indexPath: IndexPath!) -> CGFloat {
        return 15
    }
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.messages.count
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, messageDataForItemAt indexPath: IndexPath!) -> JSQMessageData! {
        return self.messages[indexPath.item]
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, didDeleteMessageAt indexPath: IndexPath!) {
        self.messages.remove(at: indexPath.row)
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, messageBubbleImageDataForItemAt indexPath: IndexPath!) -> JSQMessageBubbleImageDataSource! {
        let data = messages[indexPath.row]
        switch(data.senderId) {
        case self.senderId:
            return self.outgoingBubble
        default:
            return self.incomingBubble
        }
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, avatarImageDataForItemAt indexPath: IndexPath!) -> JSQMessageAvatarImageDataSource! {
        return nil
    }
    
    
    // MARK: Toolbar
    
    override func didPressSend(_ button: UIButton!, withMessageText text: String!, senderId: String!, senderDisplayName: String!, date: Date!) {
        guard let message = JSQMessage(senderId: senderId, senderDisplayName: senderDisplayName, date: date, text: text) else {
            fatalError()
        }
        self.messages.append(message)
        self.finishSendingMessage()
        self.socket?.emit("message", [text])
    }
    
    override func didPressAccessoryButton(_ sender: UIButton!) {
    }
    
    func setup() {
        self.senderId = ""
        self.senderDisplayName = ""
        imgBackground = UIImageView(frame: self.view.bounds)
        imgBackground?.image = #imageLiteral(resourceName: "wait")
        imgBackground?.contentMode = UIViewContentMode.scaleAspectFit
        imgBackground?.clipsToBounds = true
        self.collectionView?.backgroundView = self.imgBackground
        
    }
    override func viewDidDisappear(_ animated: Bool) {
        self.socket?.disconnect()
        
        
    }
    override func textViewDidChange(_ textView: UITextView) {
        if (textView.text.characters.count > 0) {
            self.inputToolbar.contentView.rightBarButtonItem.isEnabled = true
        }else{
            self.inputToolbar.contentView.rightBarButtonItem.isEnabled = false
        }
        // alert dialog много символов НЕЗЯ
        socket?.emit("typing", true)
        self.typingTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(TalkViewController.stopTyping), userInfo: nil, repeats: false)
    }
    func stopTyping() {
        socket?.emit("typing", false)
        self.typingTimer?.invalidate()
        self.typingTimer = nil
    }
    func waitTime(_ state:Bool){
        self.imgBackground?.isHidden = state
        self.addContactBarButton.isEnabled = state
        
    }
}
