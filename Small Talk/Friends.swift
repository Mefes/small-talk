//
//  Friends.swift
//  Small Talk
//
//  Created by Владимир Кадочников on 25.08.17.
//  Copyright © 2017 Владимир Кадочников. All rights reserved.
//

import Foundation
import SwiftyJSON
struct Friends {
    static var profiles:[Profile] = []
    init(data:AnyObject) {
        let arrayJSON = JSON(data)
        if arrayJSON.arrayObject != nil {
        for object in arrayJSON.arrayObject! {
            Friends.profiles.append(Profile.setProfile(from: object as AnyObject ))
            }
        }
        
    }
    init(data:AnyObject,req:Bool){
        
        let arrayJSON = JSON(data)
        if arrayJSON.arrayObject != nil {
            for object in arrayJSON.arrayObject! {
                Friends.profiles.append(Profile.setProfile(from: object as AnyObject ,isRequest: req))
            }
        }
        
        }
   static func removeAll() {
            Friends.profiles.removeAll()
    }
}

