//
//  ContactTableViewCell.swift
//  ttt
//
//  Created by Владимир Кадочников on 21.08.17.
//  Copyright © 2017 Владимир Кадочников. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage
class ContactTableViewCell: UITableViewCell {
    
    @IBOutlet weak private var profileImageView: RoundedImageView!
    @IBOutlet weak private var nameLabel: UILabel!
    @IBOutlet weak private var surnameLabel: UILabel!
    @IBOutlet weak private var nicknameLabel: UILabel!
    
    
    public var name:String{
        get{
            return nameLabel.text ?? "UNKNOWN"
        }set(value){
            nameLabel.text = value
        }
    }
    public var studLanguage:String{
        get{
            return surnameLabel.text ?? "UNKNOWN"
        }set(value){
            surnameLabel.text = value
        }
    }
    public var nickname:String{
        get{
            return nicknameLabel.text ?? "UNKNOWN"
        }set(value){
            nicknameLabel.text = value
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    func setIcon(iconURL:String) {
        Alamofire.request(iconURL, method: HTTPMethod.get).responseImage()
            {response in
                guard !response.result.isFailure else{return}
                self.profileImageView.image=response.result.value!
        }
    }
}
