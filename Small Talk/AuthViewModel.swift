//
//  AuthViewModel.swift
//  Small Talk
//
//  Created by Владимир Кадочников on 25.08.17.
//  Copyright © 2017 Владимир Кадочников. All rights reserved.
//

import Foundation
import RxSwift
import Alamofire
struct Auth {
    static var userID: Int?
}

class AuthViewModel {
    let response: Observable<Int>
    init(_ didPressButton:Observable<Void>,_ login:Observable<String>, _ password: Observable<String>) {
        let userInputs = Observable.combineLatest(login, password) { (login, password) -> (String, String) in
            return (login, password)
        }
        response  =   didPressButton
            .withLatestFrom(userInputs)
            .flatMap{(login,password) in
                return  Alamofire.request("http://192.168.24.169:3000/mobile_login",method: .post,parameters: [ "username":login, "password":password])
                    .authenticate(user: login, password: password)
                    .rx.responseJSON()
            }
            .map{ data in
                if let json = data as? [String:Any],
                    let ID = json["_id"] as? Int{
                    Auth.userID = ID
                    return ID
                }else{
                    return -1
                }
        }
    }
    
}
