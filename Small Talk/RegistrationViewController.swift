//
//  RegistrationViewController.swift
//  ttt
//
//  Created by Владимир Кадочников on 14.08.17.
//  Copyright © 2017 Владимир Кадочников. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
protocol RegistrationViewControllerDelegate{
    func myVCDidFinish(controller:RegistrationViewController)
}
class RegistrationViewController: UIViewController,UITextFieldDelegate ,UIPickerViewDataSource,UIPickerViewDelegate{
    let langDataSource = ["English","Русский","日本の","中國","Deutsch"]
    var ageDataSource:[Int] = []
    var delegate:RegistrationViewControllerDelegate? = nil
    @IBOutlet weak var singUpButton: UIButton!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var nicknameTextField: UITextField!
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var confirmPasswordTextField: UITextField!
    @IBOutlet weak var birthdayTextField: UITextField!
    @IBOutlet weak var sexSegmentedControl: UISegmentedControl!
    @IBOutlet weak var stutLanguageTextField: UITextField!
    //    var activeTextField:UITextField!
    let borderColor : UIColor = UIColor(red:0.87, green:0.19, blue:0.0, alpha:1.0)
    let backgrounColor : UIColor = UIColor(red:1.00, green:0.45, blue:0.30, alpha:0.2)
    override func viewDidLoad() {
        super.viewDidLoad()
        ageDataSource += 9...99
        emailTextField.delegate = self
        nicknameTextField.delegate = self
        nameTextField.delegate = self
        passwordTextField.delegate = self
        confirmPasswordTextField.delegate = self
        birthdayTextField.delegate = self
        stutLanguageTextField.delegate = self
        singUpButton.isEnabled = false
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func Cancel(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func SingUp(_ sender: UIButton) {
        if let email = emailTextField.text,
            let nickname = nicknameTextField.text,
            let name = nameTextField.text,
            let studLanguage = stutLanguageTextField.text,
            let password = passwordTextField.text,
            let confirmPassword = confirmPasswordTextField.text ,
            let birthday = birthdayTextField.text,
            let sex = sexSegmentedControl.titleForSegment(at: sexSegmentedControl.selectedSegmentIndex){
            if password == confirmPassword {
                NetworkIteraction.init().registrationRequest(nickname, name, email, birthday, sex, studLanguage, password){(success) in
                    print("Good")
                    self.dismiss(animated: true, completion: nil)
                }
            }else{
                let alert = UIAlertController(title: "Error", message: "Пароли не совпадают", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)}
        }else{
        }
    }
    
    @IBAction func setDate(_ sender: UITextField) {
        let agePickerView:UIPickerView = UIPickerView()
        agePickerView.tag = 0
        agePickerView.dataSource = self
        agePickerView.delegate = self
        sender.inputView = agePickerView
    }
    
    @IBAction func setLanguage(_ sender: UITextField) {
        let langPickerView:UIPickerView = UIPickerView()
        langPickerView.tag = 1
        langPickerView.dataSource = self;
        langPickerView.delegate = self;
        sender.inputView = langPickerView
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView.tag == 0 {
            return ageDataSource.count
        }else{
            return langDataSource.count
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView.tag == 0 {
            return "\(ageDataSource[row])"
        }else{
            return langDataSource[row]
        }
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView.tag==0 {
            birthdayTextField.text = "\(ageDataSource[row])"
            singUpButton.isEnabled = check()
            //            setStyle(birthdayTextField, with: isTextEmpty(text: birthdayTextField.text!))
        } else {
            stutLanguageTextField.text = langDataSource[row]
            singUpButton.isEnabled = check()
            //            setStyle(stutLanguageTextField, with: isTextEmpty(text: stutLanguageTextField.text!))
        }
    }
    @IBAction func passwordValidation(_ sender: UITextField) {
        singUpButton.isEnabled = check()
        setStyle(sender, with: isPasswordValid(sender.text!))
        if isPasswordValid(sender.text!) {
            
        }else{
            
        }
    }
    
    @IBAction func emailValidation(_ sender: UITextField) {
        singUpButton.isEnabled = check()
        setStyle(sender, with: isValidEmail(email: sender.text!))
    }
    @IBAction func nicknameValidation(_ sender: UITextField) {
        singUpButton.isEnabled = check()
        setStyle(sender, with: isNameLenght(name: sender.text!))
    }
    @IBAction func nameValidation(_ sender: UITextField) {
        setStyle(sender, with: isNameLenght(name: sender.text!))
        singUpButton.isEnabled = check()
    }
    @IBAction func confirmPasswordValidation(_ sender: UITextField) {
        singUpButton.isEnabled = check()
        setStyle(sender, with: isPasswordSame(password: passwordTextField.text!, confirmPassword: sender.text!))
        
    }
    
    func setStyle(_ textField:UITextField,with state:Bool) {
        textField.layer.cornerRadius=4.0;
        textField.layer.masksToBounds=true;
        if  state {
            textField.layer.borderWidth = 0.0
            textField.layer.borderColor =  UIColor.clear.cgColor
            textField.backgroundColor = UIColor.clear
        }else{
            textField.layer.borderWidth = 2.0
            textField.layer.borderColor =  borderColor.cgColor
            textField.backgroundColor = backgrounColor
        }
        //        singUpButton.isEnabled = state
    }
    func check() -> Bool{
        
        if (emailTextField.text?.contains("@"))!,
            (passwordTextField.text?.characters.count)! > 3,
            (confirmPasswordTextField.text?.characters.count)! > 3,
            passwordTextField.text == confirmPasswordTextField.text,
            (nameTextField.text?.characters.count)! > 3,
            (nicknameTextField.text?.characters.count)! > 3,
            !(birthdayTextField.text?.isEmpty)!,
            !(stutLanguageTextField.text?.isEmpty)!{
            return true
        }else{
            return false
        }
    }
}


