//
//  NetworkInteraction.swift
//  ttt
//
//  Created by Владимир Кадочников on 17.08.17.
//  Copyright © 2017 Владимир Кадочников. All rights reserved.
//

import Foundation
import Alamofire
import RxSwift

class NetworkIteraction {
    //    var profile:Profile?
    var friends: Friends?
    
    let disposeBag = DisposeBag()
    
    private var ACCESS_TOKEN:String?
    private var REFRESH_TOKEN:String?
    
    private let URL_PATH = "http://192.168.24.169:3000"
    private let REQUEST_AUTH = "mobile_login"
    private let REQUEST_USERS = "api/users/info"
    private let REQUEST_REGISTER = "api/register"
    private let REQUEST_GET_PROFILE = "api/profile"
    private let REQUEST_GET_FRIENDLIST = "api/friendlist"
    private let REQUEST_LOGOUT = "logout"
    
    struct Parameters {
        var parameters: [String: String]
        init(_ userName:String, _ password:String) {
            parameters = [
                //            "grant_type":"password",
                //            "client_id":"android",
                //            "client_secret":"SomeRandomCharsAndNumbers",
                "username":userName,
                "password":password]
        }
    }
    
    func registrationRequest(_ nickname :String,_ fullname:String,_ email :String,_ birthday :String,_ sex : String,_ studLanguage: String,_ password : String,success:@escaping(Bool)->()){
        let profile = ["username":nickname,
                       "fullname":fullname,
                       "email":email,
                       "birthday" :birthday,
                       "sex":sex,
                       "stud_lang":studLanguage,
                       "password": password]
        Alamofire.request("\(URL_PATH)/\(REQUEST_REGISTER)", method: .post, parameters: profile).responseJSON{
            response in
            if let json = response.result.value as? [String:Any],
                let statment =   json["statment"] as? String,
                statment == "OK"{
                success(true)
            }
        }
    }
    
    //    func authorizationRequest(userName:String,password:String,userID:@escaping(Int) -> ()){
    //        Alamofire.request("\(URL_PATH)/\(REQUEST_AUTH)",method: .post,parameters: Parameters.init( userName, password).parameters)
    //      .authenticate(user: userName, password: password).rx.responseJSON()
    //            .map { response -> [String:Any] in
    //                return (response as? [String:Any] ?? [:])
    //
    //        }
    //            .subscribe(onNext: {
    //                let id =   $0["_id"] as? Int ?? -1
    ////                let accessTocken =   $0["access_token"] as? String ?? ""
    ////                let refreshToken =   $0["refresh_token"] as? String ?? ""
    ////                self.getUser(from: id)
    ////                self.testGetProfile(userID: id)
    //            })
    //    }
    
    
    //    func getUser(from userID:Int) {
    //        Alamofire.request("\(URL_PATH)/\(REQUEST_GET_PROFILE)/\(userID)",method: .get).rx.responseJSON()
    //            .map { response -> [String:Any] in
    //                print("\(response)")
    //                let arrayJSON = response as? [[String:Any]] ?? [[:]]
    //                let json = arrayJSON[0] as [String:Any]
    //                        return(json)
    //            }
    //            .subscribe(onNext: { json in
    //
    //            })
    //
    //    }
    
    
    
    func getFriendList(from userID:Int){
        Alamofire.request("\(URL_PATH)/\(REQUEST_GET_FRIENDLIST)/\(userID)",method: HTTPMethod.get).rx.responseJSON()
            .map { response -> [[String:Any]] in
                print("\(response)")
                let arrayJSON = response as? [[String:Any]] ?? [[:]]
                return(arrayJSON)
            }
            .subscribe(onNext: { json in
                Friends.init(data: json as AnyObject)
                
            })
        //        .addDisposableTo(disposeBag)
        
    }
    func logOut() {
        Alamofire.request("\(URL_PATH)/\(REQUEST_LOGOUT)").response()
            { response in
                
        }
    }
    
    
    func getProfile(userID:Int,obj:@escaping (AnyObject) -> ()) {
        Alamofire.request("\(URL_PATH)/\(REQUEST_GET_PROFILE)/\(userID)",method: .get).rx.responseJSON()
            .subscribe(onNext: { response in
                let arrayJSON = response as? [[String:Any]] ?? [[:]]
                let json = arrayJSON[0] as [String:Any]
                obj(json as AnyObject)
            })
        //        .addDisposableTo(disposeBag)
    }
    
    
    
}





