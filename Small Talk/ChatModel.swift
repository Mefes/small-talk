//
//  ChatModel.swift
//  
//
//  Created by Владимир Кадочников on 28.08.17.
//
//

import Foundation
import RxSwift
import SocketIO


extension Reactive where Base: SocketIOClient{
    public func on(_ event: String) -> Observable<Any> {
        return Observable.create{observer in
            let id = self.base.on(event) { items, _ in
                observer.onNext(items)
        }
            return Disposables.create {
                self.base.off(id: id)
            }
    }
}

}
