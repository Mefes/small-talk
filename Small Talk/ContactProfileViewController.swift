//
//  ContactProfileViewController.swift
//  Small Talk
//
//  Created by Владимир Кадочников on 31.08.17.
//  Copyright © 2017 Владимир Кадочников. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage

class ContactProfileViewController: UIViewController {
    @IBOutlet weak var contactIcon: RoundedImageView!
    @IBOutlet weak var yearLable: UILabel!
    @IBOutlet weak var genderLable: UILabel!
    @IBOutlet weak var nicknameLable: UILabel!
    @IBOutlet weak var nameLable: UILabel!
    
    var fullname:String?
    var nickname:String?
    var year:String?
    var sex: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        nameLable.text! = fullname!
        nicknameLable.text! = nickname!
        yearLable.text! = year!
        genderLable.text! = sex!
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    func setIcon(iconURL:String){
        Alamofire.request(iconURL, method: HTTPMethod.get).responseImage()
            {response in
                guard !response.result.isFailure else{return}
                self.contactIcon.image=response.result.value!
        }
    }
}
//extension ContactProfileViewController{
//    
//    func getViewModel() -> ProfileViewModel{
//        let viewModel = ProfileViewModel(userID: Auth.userID!)
//        return viewModel
//    }
//}
